//rnf
import React from 'react'
import { View, Text, StyleSheet ,Image} from 'react-native'

export default function weatherInfo({info}) {
    const{
        main:{temp},
        weather:[cincai],
        name,
    } = info
    const {icon,main,description} = cincai
    const iconUrl = 'https://openweathermap.org/img/wn/' + icon+"@4x.png"
    
    return (
        <View style={styles.temperaturePosition}>
            <Text style={styles.mainFontSize}>{main}</Text>
            <Image source ={{uri:iconUrl}} style={styles.weatherIcon}/>
            <Text style={styles.place}>{name}</Text>
            <Text>{info.main.temp}°C | <Text>{description}</Text></Text>
        </View>
    )
}

const styles = StyleSheet.create({
    temperaturePosition:{
        alignItems:'center'
    },
    weatherIcon:{
        width:100,
        height:100
    },
    mainFontSize:{
        fontSize:25,
        textTransform:'uppercase'
    },
    place:{
        fontWeight:'700'
    }
})
